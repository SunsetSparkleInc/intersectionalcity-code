// Fill out your copyright notice in the Description page of Project Settings.

#include "SexyCityGameInstance.h"

#include "Engine.h"

#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "PressurePlate.h"
#include "MenuSystem/MainMenu.h"
#include "MenuSystem/MenuWidget.h"

const static FName SESSION_NAME = TEXT("Session Zero");
const static FName SERVER_NAME_SETTINGS_KEY = TEXT("ServerName");

USexyCityGameInstance::USexyCityGameInstance(const FObjectInitializer & ObjectInitializer) {
    ConstructorHelpers::FClassFinder<UUserWidget> MenuBPClass(TEXT("/Game/MenuSystem/WBP_MainMenu"));
    if (!ensure(MenuBPClass.Class != nullptr)) return;

    MenuClass = MenuBPClass.Class;

    ConstructorHelpers::FClassFinder<UUserWidget> InGameMenuBPClass(TEXT("/Game/MenuSystem/WBP_InGameMenu"));
    if (!ensure(InGameMenuBPClass.Class != nullptr)) return;

    InGameMenuClass = InGameMenuBPClass.Class;

    UE_LOG(LogTemp, Warning, TEXT("Found Class %s"), *MenuBPClass.Class->GetName());
}

void USexyCityGameInstance::Init() {
    IOnlineSubsystem* Subsystem = IOnlineSubsystem::Get();
    if(Subsystem != nullptr) {
        UE_LOG(LogTemp, Warning, TEXT("Found subsystem %s"), *Subsystem->GetSubsystemName().ToString());
        SessionInterface = Subsystem->GetSessionInterface();
        if (SessionInterface.IsValid()) {
            SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &USexyCityGameInstance::OnCreateSessionComplete);
            SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &USexyCityGameInstance::OnDestroySessionComplete);
            SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &USexyCityGameInstance::OnFindSessionsComplete);
            SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &USexyCityGameInstance::OnJoinSessionComplete);
        }
    } else {
        UE_LOG(LogTemp, Warning, TEXT("Found no subsystem"));
    }
    UE_LOG(LogTemp, Warning, TEXT("Found Init Class %s"), *InGameMenuClass->GetName());
}

void USexyCityGameInstance::LoadMenu() {
    if (!ensure(MenuClass != nullptr)) return;

    Menu = CreateWidget<UMainMenu>(this, MenuClass);
    if (!ensure(Menu != nullptr)) return;

    Menu->Setup();

    Menu->SetMenuInterface(this);
}

void USexyCityGameInstance::InGameLoadMenu()
{
    if (!ensure(InGameMenuClass != nullptr)) return;

    UMenuWidget* GameInstanceMenu = CreateWidget<UMenuWidget>(this, InGameMenuClass);
    if (!ensure(GameInstanceMenu != nullptr)) return;

    GameInstanceMenu->Setup();

    GameInstanceMenu->SetMenuInterface(this);
}

void USexyCityGameInstance::Host()
{
    if (SessionInterface.IsValid())
    {
        auto ExistingSession = SessionInterface->GetNamedSession(SESSION_NAME);
        if (ExistingSession != nullptr)
        {
            SessionInterface->DestroySession(SESSION_NAME);
        }
        else
        {
            CreateSession();
        }
    }
}

void USexyCityGameInstance::CreateSession()
{
    if (SessionInterface.IsValid()) {
        FOnlineSessionSettings SessionSettings;
        if (IOnlineSubsystem::Get()->GetSubsystemName() == "NULL")
        {
            SessionSettings.bIsLANMatch = true;
        }
        else
        {
            SessionSettings.bIsLANMatch = false;
        }
        SessionSettings.NumPublicConnections = 5;
        SessionSettings.bShouldAdvertise = true;
        SessionSettings.bUsesPresence = true;
        //SessionSettings.bUseLobbiesIfAvailable = true;

        SessionInterface->CreateSession(0, SESSION_NAME, SessionSettings);
    }
}

void USexyCityGameInstance::OnCreateSessionComplete(FName SessionName, bool Success){
    if (!Success) {
        UE_LOG(LogTemp, Warning, TEXT("Could not create session"));
        return;
    }
    UEngine* Engine = GetEngine();
    if(!ensure(Engine != nullptr)) return;

    Engine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("Hosting"));

    UWorld* World = GetWorld();
    if(!ensure(World != nullptr)) return;

    World->ServerTravel("/Game/ThirdPersonBP/Maps/ThirdPersonExampleMap?listen");
}

void USexyCityGameInstance::OnDestroySessionComplete(FName SessionName, bool Success) {
    if (Success) {
        CreateSession();
    }
}

void USexyCityGameInstance::RefreshServerList()
{
    SessionSearch = MakeShareable(new FOnlineSessionSearch());
    if (SessionSearch.IsValid()) {
        //SessionSearch->bIsLanQuery = true;
        SessionSearch->MaxSearchResults = 100;
        SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
        UE_LOG(LogTemp, Warning, TEXT("Starting Find Session"));
        SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
    }
}

void USexyCityGameInstance::OnFindSessionsComplete(bool Success) {
    if (Success && SessionSearch.IsValid() && Menu != nullptr) {
        UE_LOG(LogTemp, Warning, TEXT("Finished Find Session"));
        TArray<FString> ServerNames;
        ServerNames.Add("Session One");
        ServerNames.Add("Session Two");
        ServerNames.Add("Session Three");
        for (const FOnlineSessionSearchResult& SearchResult : SessionSearch->SearchResults) {
            UE_LOG(LogTemp, Warning, TEXT("Found Session Named: %s"), *SearchResult.GetSessionIdStr());
            ServerNames.Add(*SearchResult.GetSessionIdStr());
        }
        Menu->SetServerList(ServerNames);
    }
}

void USexyCityGameInstance::Join(uint32 Index){

    if (!SessionInterface.IsValid()) return;
    if (!SessionSearch.IsValid()) return;

    if (Menu != nullptr) 
    {
        Menu->Teardown();
    }

    SessionInterface->JoinSession(0, SESSION_NAME, SessionSearch->SearchResults[Index]);

   /* */
}

void USexyCityGameInstance::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{

    if (!SessionInterface.IsValid()) return;

    FString Address;
    if (!SessionInterface->GetResolvedConnectString(SessionName, Address)) {
        UE_LOG(LogTemp, Warning, TEXT("Could not get connect string"));
        return;
    }

    UEngine* Engine = GetEngine();
    if (!ensure(Engine != nullptr)) return;

    Engine->AddOnScreenDebugMessage(0, 2, FColor::Yellow, FString::Printf(TEXT("Joining %s"), *Address));

    APlayerController* PlayerController = GetFirstLocalPlayerController();
    if (!ensure(PlayerController != nullptr)) return;

    PlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
}

void USexyCityGameInstance::StartSession()
{
    if (SessionInterface.IsValid())
    {
        SessionInterface->StartSession(SESSION_NAME);
    }
}

void USexyCityGameInstance::LoadMainMenu()
{
    APlayerController* PlayerController = GetFirstLocalPlayerController();
    if (!ensure(PlayerController != nullptr)) return;

    PlayerController->ClientTravel("/Game/MenuSystem/MainMenu", ETravelType::TRAVEL_Absolute);
}