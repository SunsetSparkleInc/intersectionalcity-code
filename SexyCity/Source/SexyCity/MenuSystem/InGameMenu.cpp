// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameMenu.h"

//bool UInGameMenu::Initialize() {
	//bool Success = Super::Initialize();
	//if (!Success) return false;

	//if (!ensure(CancelInGameMenu != nullptr)) return false;
	//CancelInGameMenu->OnClicked.AddDynamic(this, &UMainMenu::JoinServer);

	//return true;
//}

#include "Components/Button.h"

bool UInGameMenu::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(InGameCancelButton != nullptr)) return false;
	InGameCancelButton->OnClicked.AddDynamic(this, &UInGameMenu::CancelPressed);

	if (!ensure(InGameMainMenuButton != nullptr)) return false;
	InGameMainMenuButton->OnClicked.AddDynamic(this, &UInGameMenu::QuitPressed);

	return true;
}

void UInGameMenu::CancelPressed()
{
	Teardown();
}


void UInGameMenu::QuitPressed()
{
	if (MenuInterface != nullptr) {
		Teardown();
		MenuInterface->LoadMainMenu();
	}
}